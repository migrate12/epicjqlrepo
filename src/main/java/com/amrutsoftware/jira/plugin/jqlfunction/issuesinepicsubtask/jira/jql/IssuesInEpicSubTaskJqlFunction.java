package com.amrutsoftware.jira.plugin.jqlfunction.issuesinepicsubtask.jira.jql;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.sun.webkit.plugin.PluginListener;

/**
 * Echoes the the string passed in as an argument.
 */
public class IssuesInEpicSubTaskJqlFunction extends AbstractJqlFunction {
	private static final Logger log = LoggerFactory.getLogger(IssuesInEpicSubTaskJqlFunction.class);

	public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand,
			TerminalClause terminalClause) {
		List<QueryLiteral> literals = new LinkedList<QueryLiteral>();
		String epicLink = operand.getArgs().get(0);
		String epicLinkValue = epicLink.trim().substring(epicLink.lastIndexOf("=") + 1).trim();
		if (!ComponentAccessor.getIssueManager().getIssueByCurrentKey(epicLinkValue).getSubTaskObjects().isEmpty()) {
			for (Issue subTask : ComponentAccessor.getIssueManager().getIssueByCurrentKey(epicLinkValue)
					.getSubTaskObjects())
				literals.add(new QueryLiteral(operand, subTask.getId()));
		}
		String conditionQuery = "\"Epic Link\" = " + epicLinkValue;
		JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
		PagerFilter pagerFilter = PagerFilter.getUnlimitedFilter();
		SearchResults searchResults = null;
		try {
			Query query = ComponentAccessor.getComponent(JqlQueryParser.class).parseQuery(conditionQuery);
			searchResults = ComponentAccessor.getComponent(SearchService.class)
					.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, pagerFilter);
		} catch (SearchException e) {
			e.printStackTrace();
		} catch (JqlParseException e) {
			e.printStackTrace();
		}
		List<Issue> issues1 = searchResults.getIssues();
		for (Issue issue1 : issues1) {
			literals.add(new QueryLiteral(operand, issue1.getId()));
			if (!issue1.getSubTaskObjects().isEmpty()) {
				for (Issue issue2 : issue1.getSubTaskObjects()) {
					literals.add(new QueryLiteral(operand, issue2.getId()));
				}
			}
		}
		return literals;
	}

	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

	@Override
	public MessageSet validate(ApplicationUser arg0, FunctionOperand operand, TerminalClause arg2) {
		// TODO Auto-generated method stub
		return validateNumberOfArgs(operand, 1);
	}
}