package ut.com.amrutsoftware.jira.plugin.jqlfunction.issuesinepicsubtask;

import org.junit.Test;
import com.amrutsoftware.jira.plugin.jqlfunction.issuesinepicsubtask.api.MyPluginComponent;
import com.amrutsoftware.jira.plugin.jqlfunction.issuesinepicsubtask.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}